class App {
    public int removeDuplicates(char[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int k = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }

        for (int j = k; j < nums.length; j++) {
            nums[j] = '-';
        }

        return k;
    }

    public void printArray(char[] nums) {

        System.out.print("Input: nums = [");
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
            if (i < nums.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println();
        int k = removeDuplicates(nums);
        System.out.print("Output: " + k + ", nums = [");
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
            if (i < nums.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println();
    }

    public static void main(String[] args) {
        App app = new App();

        char[] nums1 = { '1', '1', '2' };
        app.printArray(nums1);

        char[] nums2 = { '0', '0', '1', '1', '1', '2', '2', '3', '3', '4' };
        app.printArray(nums2);
    }
}
